import sys
import urllib
import urlparse
import os
import xbmcgui
import xbmcplugin
import xbmcaddon

from sgmllib import SGMLParser

class URLLister(SGMLParser):
	def reset(self):
		SGMLParser.reset(self)
		self.urls = []

	def start_a(self, attrs):
		href = [v for k, v in attrs if k=='href']
		if href:
			self.urls.extend(href)

base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
args = urlparse.parse_qs(sys.argv[2][1:])

__addon__       = xbmcaddon.Addon()
__addonname__   = __addon__.getAddonInfo('name')
__icon__        = __addon__.getAddonInfo('icon')

xbmcplugin.setContent(addon_handle, 'movies')

def build_url(query):
	return base_url + '?' + urllib.urlencode(query)

def GUIEditExportName(name):

	exit = True 
	while (exit):
		kb = xbmc.Keyboard('default', 'heading', True)
		kb.setDefault('')
		kb.setHeading(name)
		kb.setHiddenInput(False)
		kb.doModal()
		if (kb.isConfirmed()):
			name_confirmed  = kb.getText()
			name_correct = name_confirmed.count(' ')
			
			if (name_correct):
				GUIInfo(2,'a space is not allowed') 
			else: 
				name = name_confirmed
				exit = False
		else:
			GUIInfo(2,'keyboard was closed') 
			
	return(name)

mode = args.get('mode', None)

if mode is None:

	url = build_url({'mode': 'enter'})
	li = xbmcgui.ListItem('Add URL', iconImage='DefaultFolder.png')
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

	url = build_url({'mode': 'page', 'url': 'http://www.speedtyping.net/dl/'})
	li = xbmcgui.ListItem('http://www.speedtyping.net/dl', iconImage='DefaultFolder.png')
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

	#url = build_url({'mode': 'add', 'url': 'rtsp://10.0.0.5:554/ch0_0.h264'})
	#url = 'rtsp://192.168.1.69:554/ch0_0.h264'
	url = 'rtsp://10.0.0.5:554/ch0_0.h264';
        li = xbmcgui.ListItem('Alex Cam', iconImage='DefaultFolder.png')
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)

#	otherurls = __addon__.getSetting('otherurls')
#
#	for sourceurl in otherurls:
#		url = build_url({'mode': 'page', 'url': sourceurl})
#		li = xbmcgui.ListItem(sourceurl, iconImage='DefaultFolder.png')
#		xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder=True)

	xbmcplugin.endOfDirectory(addon_handle)

elif mode[0] == 'enter':

	url = GUIEditExportName('Enter URL to add')

	sock = urllib.urlopen(url)
	dlpage = sock.read()
	sock.close()

	parser = URLLister()
	parser.feed(dlpage)
	parser.close()

	filetypes = ['avi', 'mp4', 'mkv', 'mov', 'm4a', 'flv', 'webm', 'vob', 'ogg', 'wmv', 'rmvb', 'mpg', 'mpeg', '3gp', 'cdg', 'mp3']

	for url in parser.urls:
		for filetype in filetypes:
			if url.endswith('.'+filetype):
				li = xbmcgui.ListItem(os.path.basename(url), iconImage='DefaultVideo.png')
				xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)

	xbmcplugin.endOfDirectory(addon_handle)

elif mode[0] == 'add':
	#my_addon.setSetting('my_setting', 'false')

	url = GUIEditExportName('Enter URL to add')
	
	otherurls = __addon__.getSetting('otherurls')
	otherurls.append(url)
	__addon__.setSetting('otherurls', otherurls)
	
	line1 = "New url has been added"
	time = 5000

	xbmc.executebuiltin('Notification(%s, %s, %d, %s)'%(__addonname__,line1, time, __icon__))

elif mode[0] == 'page':
	url = args['url'][0]
	sock = urllib.urlopen(url)
	dlpage = sock.read()
	sock.close()

	parser = URLLister()
	parser.feed(dlpage)
	parser.close()

	filetypes = ['avi', 'mp4', 'mkv', 'mov', 'm4a', 'flv', 'webm', 'vob', 'ogg', 'wmv', 'rmvb', 'mpg', 'mpeg', '3gp', 'cdg', 'mp3']

	for url in parser.urls:
		for filetype in filetypes:
			if url.endswith('.'+filetype):
				li = xbmcgui.ListItem(os.path.basename(url), iconImage='DefaultVideo.png')
				xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)

	xbmcplugin.endOfDirectory(addon_handle)
